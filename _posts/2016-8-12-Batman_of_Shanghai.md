---
date: 2016-08-12 16:36:34-05:00
embed: <iframe width="480" height="270" src="https://www.youtube.com/embed/dA6KPuUGQz4?feature=oembed"
  frameborder="0" allowfullscreen></iframe>
embed_url: https://youtu.be/dA6KPuUGQz4
layout: post
tags:
- animation
- awesome
- Batman
timestamp: 1471037794
title: Batman of Shanghai
type: video

---
This is amazing. I want an entire series, and a companion comic, and a feature-length film.